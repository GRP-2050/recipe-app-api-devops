variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email@test123.com"
}

variable "db_username" {
  description = "Username for RDS prosgress instance"
  
}

variable "db_password" {
  description = "Password for RDS prosgress instance"
  
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
  
}